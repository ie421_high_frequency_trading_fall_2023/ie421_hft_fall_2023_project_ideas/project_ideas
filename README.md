# IE421 Fall 2023 Project Ideas

- [IE421 Fall 2023 Project Ideas](#ie421-fall-2023-project-ideas)
  - [Description](#description)
  - [Proposed Projects](#proposed-projects)
    - [Proposed Project: HFT-Technique Enhanced GPS Grandmaster on Raspberry Pi 4](#proposed-project-hft-technique-enhanced-gps-grandmaster-on-raspberry-pi-4)
      - [Raspberry PI GPS Tutorials](#raspberry-pi-gps-tutorials)
      - [Basic Techniques](#basic-techniques)
      - [Slightly More Advanced Techniques](#slightly-more-advanced-techniques)
      - [More Advanced techniques:](#more-advanced-techniques)
    - [Proposed Project: Investigation of U-Blox GPS Receiver Time Sync Best Practices and Performance](#proposed-project-investigation-of-u-blox-gps-receiver-time-sync-best-practices-and-performance)
      - [Example Commercial GPS PTP NTP Grandmaster Devices](#example-commercial-gps-ptp-ntp-grandmaster-devices)
      - [Potential Project Target GPS Receiver Examples](#potential-project-target-gps-receiver-examples)
        - [U-Blox GPS Timing Enhancement Features](#u-blox-gps-timing-enhancement-features)
    - [Proposed Project: 3D Web-based Visualization of Multiple GNSS Receivers](#proposed-project-3d-web-based-visualization-of-multiple-gnss-receivers)
      - [Description:](#description-1)
    - [Proposed Project - Tick Database in the Lab](#proposed-project---tick-database-in-the-lab)
      - [Description](#description-2)
    - [Propose Project - Lab Monitoring and Protection System](#propose-project---lab-monitoring-and-protection-system)
      - [Description:](#description-3)
    - [Proposed Project - High Packet Rate Network Capture System](#proposed-project---high-packet-rate-network-capture-system)
      - [Description:](#description-4)
    - [Proposed Project - CI/CD Lab Infrastructure](#proposed-project---cicd-lab-infrastructure)
      - [Description:](#description-5)
    - [Proposed Project (Hard / Reach Project) - Add Kernel Bypass Support to Rpi 4](#proposed-project-hard--reach-project---add-kernel-bypass-support-to-rpi-4)
      - [Description:](#description-6)


## Description

This project contains a list and in some cases detailed descriptions of proposed potential student projects for IE421. 

Based on satisfactory progress, some of the proposed projects could easily be continued as part of an IE497 / IE597 (or other department equivalent) semester long independent project for more credit next semester. 

## Proposed Projects


### Proposed Project: HFT-Technique Enhanced GPS Grandmaster on Raspberry Pi 4

Note: the Professor REALLY wants at least one group to do THIS project! It could easily turn into an excellent published paper, and I will personally help you with it. 

The goal of this project is to use numerous commonly applied "HFT techniques" for enhancing performance and reducing variability / increasing determinism and applying them to utilizing raspberry pi 4s, first as a GPS grandmaster, and then others as PTP based clients which can sync to that grandmaster. 

It is very easy to turn a raspberry pi 4 into a GPS grandmaster using only a few simple commands and a GPS receiver from u-blox that costs between $7-$30. 

#### Raspberry PI GPS Tutorials
For a brief tutorial, see:
- [ ] [Microsecond accurate NTP with a Raspberry Pi and PPS GPS](https://austinsnerdythings.com/2021/04/19/microsecond-accurate-ntp-with-a-raspberry-pi-and-pps-gps/)
- [ ] [GPS Raspberry Pi NTP Server](https://blog.networkprofile.org/gps-backed-local-ntp-server/)
- [ ] [Raspberry Pi GPS Time Server with Bullseye](https://n4bfr.com/2021/11/raspberry-pi-gps-time-server-with-bullseye/)
- [ ] [GPS disciplined Stratum-1 NTP Server with Raspberry PI - a HowTo](https://github.com/domschl/RaspberryNtpServer)
- [ ] [Build a Pi Time Server With GPS & Kernel PPS](https://robrobinette.com/pi_GPS_PPS_Time_Server.htm)

Once gpsd is working and communicating with chrony, it is easy to monitor the "PPS offset" to get a sense of each time the GPS chip raises the Pulse-per-Second pin "High", generating an interrupt, how close or far the local time was. This is essentially exactly how much much more expensive higher end GPS PTP / NTP Grandmasters used in the Financial Services industry also function (albeit with slightly different hardware and possibly other software packages, whether opensource or proprietary).

Note that while you are likely not familiar with the techniques below, many are actually quite simple, will be covered in class and probably in an upcoming group homework assignment with networking, and are actually extremely quick to implement (either modifying a single file with a few extra parameters, or running a few simple bash scripts).

#### Basic Techniques
The basic list of techniques to attempt to tune the performance:
- isol_cpu kernel parameter so that all processes by default run on the 1st of the four raspberry pi cores (leaving the other three free from any processes or software by default)
- Pinning individual time-related processes each to their own dedicated core(s) (we will need to check the threading model to see if multi-threaded or not and if so, which threads are actually in the critical path for timing)
- interrupt steering so that PPS output IRQ goes to either the core running whichever daemon handles the PPS (GPSD or chrony presumably) or another dedicated core

#### Slightly More Advanced Techniques
- Switching from interrupt driven to busy poll driven detection of the PPS output (my understanding is these are mapped to memory locations so you can just continiously busy spin in a dedicated core to detect the first clock cycle (every second) where the PPS value goes high, indicating a new on-rise)
- examining recompiling some of the libraries / daemons to alter parameters like size vs speed performance (sometimes it is more important to have deterministic timing by keeping assembly code sizes smaller than necessarily the fastest, because smaller code more likely to fit into cache)
- similarly optimizing code to improve performance (stripping out some unnecessary logging, swapping out or eliminating less efficient functions)
- utilizing special ARM assembly instructions to control caching so that certain data structures and / or code stay in cache and others are never cached.
- Ensuring compilation is targeting the exact chip being used and not just a generic ARM core (which may not support all of the features that are actually available on that raspberry pi 4 ARM specifically)
- tuning other items that can determine variability in run time like the frequency governor or thermal controls on the raspberry pi. Often modern CPUs will dynamically adjust their CPU clock speed in response to both temperature and to real time use

#### More Advanced techniques:
- Utilizing the "offset" data outputted by the GPS receiver that corrects for the known error in when the PPS output will actually come (this has to do with the GPS chip itself not clocked at 1GHz and therefore it doesn't detect the "on-one-second" state until potentially after it's occurred when it wakes up while running on a 25-50MHz clock)
- Utilizing custom busy spin code on yet other (non critical cores) to actually generate heat as part of a "thermal regulation" of the air around the raspberry pi and GPS receiver. The more thermally stable the environment, theoretically, the better.
- Utilizing the PPS output from MANY GPS receivers, each connected to a single pin on the raspberry pi. We'd need to talk with U-Blox on details, but the intuition is that the earliest or some minimum number toggled would be treated as the signal rather than relying on only a single receiver. If nothing else, getting statistics on how different chips, all located nearby, would be quite interesting. 


***
***

### Proposed Project: Investigation of U-Blox GPS Receiver Time Sync Best Practices and Performance

The goal of this project is to research, experiment, and measure various best practices for deploying and optimzing U-Blox GPS Receivers as time synchronization providers. 

In high frequency trading (and electronic finance in general), it is extremely often necessary to synchronize time across multiple geographically distributed data centers, sometimes kilometers apart, sometimes on other sides of the planet. 

The gold standard for doing this is utilizing GPS Receivers which provide a pulse-per-second (PPS) which is literally a coaxial wire that "goes high" one a second as close to the nearest nanosecond as possible, so for example at 12:01:04.000000000 and then again at 12:01:05.000000000, etc, etc. 

In addition the GPS receiver also feeds out the current date and time. Together, these two can be used to very accurately provide time synchronization locally, which can then be fed to external devices via PPS (gold standard), PTP (over the network), and NTP (legay over the network). 

#### Example Commercial GPS PTP NTP Grandmaster Devices

As an example of such devices see:
- [ ] [Safran (acquired from Spectracom) SECURESYNC® TIME SERVER](https://safran-navigation-timing.com/product/securesync-time-and-frequency-reference-system/?model_interest__c=SecureSync&product_interest___single_select=Resilient+Timing)
- [ ] [Microchip (acquired from Symmetricom) TimeProvider® 5000 Grandmaster Clock](https://www.microchip.com/en-us/products/clock-and-timing/systems/ptp-grandmaster-clocks/timeprovider-5000)


#### Potential Project Target GPS Receiver Examples

The intended devices we will most likely be using include:
- [ ] [u-blox LEA-M8T](https://www.u-blox.com/en/product/neolea-m8t-series)
- [ ] [u-blox ZED-F9T](https://www.u-blox.com/en/product/zed-f9t-module)

The advantage of the 8th and 9th generation u-blox receivers is that they are "dual frequency" receivers, which among others can receive both the L1 and L5 signals (you don't know what those are yet but don't worry). The reason this is important is that the real time weather dynamics can alter regions of the earth's atmosphere (iosphere, stratosphere, etc), and this in turn can alter the speed of light (including GNSS signals). Because there is a frequency dependance on this alteration, having two (ideally not too close together) frequencies allows for determining and then correcting for ionspheric distortions. 


The goal of this project to investigate methods of characterizing time quality (mainly just output from the existing tools) and test out various advanced features of the receivers to improve timing. 

##### U-Blox GPS Timing Enhancement Features
- Utilizing u-blox's windows-based [u-center](https://www.u-blox.com/en/product/u-center) for monitoring and initially configuring the GPS receiver. 
- Utilzing either existing software or writing your own simple serial-console software to also receive and send generic NMEA and u-blox specific messages to the GPS receivers
- Performing averaging of the detected GPS location and then switching the GPS receiver into fixed "timing mode" (where you actually provide the exact GPS coordinates and then it will only solve for time, potentially using only a single GPS satellite); this will also speed up the time till "initial lock" (when you first power on the device until it actually starts outputting time and PPS)
- Utilizing the ionospheric correction values from third party services to be injected into the receiver (U-blox provides an API for this):
    - [u-blox PointPerfect](https://www.u-blox.com/en/technologies/ppp-rtk-gnss-correction-services-pointperfect)
    - [Septentrio](https://customersupport.septentrio.com/s/article/Using-Septentrio-s-Receiver-and-uBlox-s-correction-services-for-precise-positioning)
    - Note I asked ChatGPT and (after some poking with its ommissions) it provided this list, but who knows if they're all real or hallucinations: 
      - Trimble RTX
      - NovAtel CORRECT with PPP
      - Leica SmartLink
      - Topcon T-Net
      - Fugro OmniSTAR
      - Septentrio PPP-RTK
      - TerraStar
      - StarFire Network by John Deere
      - Veripos
      - Hemisphere Atlas
      - u-blox PointPerfect
- Explore other GNSS correction / enhancement services (see list above)
- Utilize the rover-base mode (where two GPS receivers that are within 10s of KMs apart) can be used to provide a more accurate relative synchronization between the two, given they are both subject to the same ionspheric and other distortions
- Attempt to export the raw GNSS values and then use third party software for solving out of band
- Compare utilizing specific GNSS constallations (GPS vs GLONASS vs Galileo, etc)
- Examining GPS performance as a function temperature variations in the local hardware environment (can easily use raspberry pi both directly and with add sensors to measure the temperature and humidity in real time)
- Examining various environmental conditions that may impact timing synchronization (real time weather, location of satellites, time of day, etc)
- other ideas that your team will come up with by researching the topic of GNSS synchronziation, reading through various manufacturer data sheets, etc. 

***
---


### Proposed Project: 3D Web-based Visualization of Multiple GNSS Receivers

#### Description:
This project is relatively easy to describe: implement a database-backed web-based version of [PyGPSClient](https://github.com/semuconsulting/PyGPSClient) - see the screenshots. The additional goal would be to handle data and communications with multiple raspberry pis which can be viewed in a single location. 

The goal would be to write a simple local python script that could be run on multiple raspberry pis with GPS receivers and capture and upload the data to a database / webserver with an interactive web app providing a dashboard of each of the GPS receivers. 

One of the reasons this is useful: many electronic trading firms will have multiple data centers around the globe, each with different views of the sky (and therefore different GNSS satellites in view). This will allow viewing globally in a single location all of their receivers and all of the telemetry and health of each of those receivers, the various constellations and satellites, etc. 

Also, the goal would be to (similar to PyGPSClient) allow configuration of the GPS receivers via custom u-blox commands (the description of the prior project to understand this). Thus this isn't only for viewing but also managing / configuring the receivers. 

Therefore this project allows both viewing, monitoring, and configuring a globally distributed set of GPS receivers from a single web-based interface.

As part of the "visualization" aspect, an ideal project would provide sensor fusion, allowing to visualize both:
- locations of all of the GNSS receivers
- locations of all the (at least observed) satellites above the earth
- local weather 

Future enhancements of this (later semesters):
- microwave, millimeterwave, and HF transmitters / paths
- airplanes
- ships 
- cell phone towers
- data centers

There are already some very impressive opensource 3D web-based libraries that (on first glance) would be very useful for this:
- [CesiumJS](https://sandcastle.cesium.com/)



### Proposed Project - Tick Database in the Lab

#### Description

OneTick, one of the major commercial providers of financial time series data specific databases, has previously offered to donate their product to our program. 

Previously, I did not have any permenant location with sufficient resources to reasonably deploy it. Now with the lab, we do. 

The goal of this project is to:
- bring up OneTick on a VM located in the lab (using DevOps)
- Utilize existing market data parsers to load up market data into OneTick
- Develop or locate other market data parsers and fetch scripts to load additional sources of data into the OneTick instance
- Integrate OneTick database with our backtesting software (StrategyStudio - it already supports it)
- Develop various example tutorials and scripts for utilzing their API (pulling various amounts of data with python, loading into pandas, visualizing with various plotting libraries like matplotlib, etc)


### Propose Project - Lab Monitoring and Protection System

#### Description:

A standard best-practice when operating large fleets of computer hardware, networking gear, and other devices, is to deploy automated monitoring software for gathering, storing, visualizing, analyzing, and alerting across a variety of important metrics related to both operational integrity (making sure hardware isn't about to fail, software is properly configured) and cybersecurity (attempting to detect hacking attempts, detecting out of date software or firmware that could be exploited or hacked, etc).

Examples of common items measured and recorded:
- packet statistics (packets sent, received, dropped, corrupted, etc)
- temperatures (of CPUs, internal case temps, memory, GPUs, etc)
- Disk I/O (number of disk reads / writes both in # and MB/s transferred)
- Security events (attempted / failed logins, succesful logins, etc)
- System error events: corrupted RAM, hard drive counter failures, temperatures exceeding safe limits, etc

The goal of this project is both setup and configure numerous data center monitoring software packages. Some will be specific to "operations", see [Zabbix](https://www.zabbix.com/), while others may be specific more to the security side of monitoring.

For a list of security related software, here is a brief (ChatGPT) generated list, many of which are of interest:
- SIEM (Security Information and Event Management)
- EDR (Endpoint Detection and Response)
- Firewall Management
- Vulnerability Scanners
- IDS/IPS (Intrusion Detection/Prevention Systems)
- DLP (Data Loss Prevention)
- IAM (Identity and Access Management)
- Web Application Firewalls (WAF)
- Threat Intelligence Platforms
- VPN (Virtual Private Network)
- MDM (Mobile Device Management)
- Patch Management Systems
- CASB (Cloud Access Security Brokers)
- Secure Email Gateways
- Encryption Tools

There are also opensource projects related to many of the above. 

The general goal is this project is an excellent broad exposure for students to DevOps and cloud / datacenter system engineering, deployment, and monitoring. 


### Proposed Project - High Packet Rate Network Capture System

#### Description:

The name says most of it. This project involves properly optimizing and configuring linux to correctly handle capturing extremely highrates of incoming packets and writing them to disk without dropping. 

There are numerous tools we will start with to do this (simplest being tcpdump, but very quickly moving on solarflare's own capture tool). 

The more interesting two steps are:
- How to safetly configure the linux system to ensure that buffers are sufficiently large both on the NICs and the filesystems so that packets are not dropped (given sufficient RAM). 
- How to in "close to real time" synchronize this captured data to remote sites (other computers) without dropping packets locally. 

This is exactly the type of project that virtually every HFT firm has either purchased or more likely built in house from scratch. 

Much of the required tuning is already documented in detail by Solarflare (purchased by Xilinx then purchased by AMD). See their [SolarCapture](https://www.xilinx.com/content/dam/xilinx/publications/solarflare/drivers-software/solarcapture-pro/activate/v1-6-3/SF-108469-CD-11_SolarCapture_User_Guide.pdf) especially the section titled "Tuning Guide" that describes best practices for configuring systems to properly handle capturing and writing to disk large amounts of data. 


### Proposed Project - CI/CD Lab Infrastructure

#### Description:

This project involves building out the DevOps stack and associoted tools to be used for automating deployment and testing of both Virtual Machines AND Physical machines in the lab to automatically build, deploy, and test various student projects. 

This group will work closely with the Professor to deploy CI/CD infrastructure such as [Jenkins](https://www.jenkins.io/) so that going forward students' projects for this and other courses can be automatically checked and run in the lab. The intent is to provide batch task scheduling framework (utilizing Jenkins), to allow for seamless sharing of lab physical infrastructure. 

This will build off of prior student work in also using diskless network booting of various physical machines to allow for stateless automated provisioniing and bring up of physical machines that can then run various projects on bare-metal in a fully automated fashion in which the state of the machines is completely known (since the images being booted will be entirely controlled through git providing completely deterministic system state). 

This is an extremely real world project that many HFT firms (myself included) have built internally for automating HFT Performance labs to provide a way to version control and test out new configurations for optimzizing both software and hardware performance in a lab setting before migrating those settings to production. 

By doing this project, you will also work with the professor to gain more exposure to more HFT tuning best practices, as one of the first "test setups" you will be implementing on top of this as an example will be low latency linux configurations between at least two nodes. 

### Proposed Project (Hard / Reach Project) - Add Kernel Bypass Support to Rpi 4

#### Description:

The goal of this project is to modify / extend the exsiting rpi 4 ethernet driver to enable support for XDP. We will cover these types of technologies in much more detail in the semester, but a basic (ChatGPT) summmary of XDP:

```eXpress Data Path (XDP) is an advanced feature integrated into the Linux kernel, designed to offer high-speed packet processing capabilities. By utilizing BPF (Berkeley Packet Filter) bytecode, XDP allows for the implementation of custom, low-level networking functions that execute directly on the NIC driver's receive path, right after the NIC DMA's the packets into RAM, but before the kernel networking stack takes over. This results in minimal latency and CPU utilization, providing a performance-optimized solution for use cases such as packet filtering, routing, load balancing, and DoS mitigation. XDP programs can modify packets in-place, make forwarding decisions, or even drop packets altogether, executing complex logic with negligible performance overhead. Because it's part of the Linux kernel, XDP is highly extensible, offering integration with other kernel subsystems and user-space applications, while its reliance on BPF ensures safety, as all XDP programs must pass verification checks before execution.```


For a basic youtube tutorial in modifying ethernet NIC drivers to enable XDP, see [this video](https://www.youtube.com/watch?v=ayFWnFj5fY8).

Someone may have already implemented this, see [Galette](https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=10186412&casa_token=rfBbJs4PXmwAAAAA:qUbaTktpdZAMduzfTASVAFHzT3tfhZeib1ASl4stfwQn1QJKT4FTFBTxTo8_D0dbvaVvekOAhJ8&tag=1)

Depending on what is already implemented, this project could also turn into a performance benchmarking and survey of existing kernel-bypass low latency networking options specifically on the raspbery pi 4. 